package tv.loqoo.v4;

import io.iron.ironmq.Client;
import io.iron.ironmq.Cloud;
import io.iron.ironmq.Message;
import io.iron.ironmq.Queue;
import java.io.IOException;
import com.amazonaws.util.json.JSONObject;
import android.os.AsyncTask;
import android.util.Log;


public class IronMQ extends AsyncTask<String, String, JSONObject> {

public String projectId;
public String token;
public String q;
public String message;
public Queue queue;

	@Override
	protected void onPreExecute() {
		super.onPreExecute();
	}

	@Override
	protected JSONObject doInBackground(String... params) {
		try {
			Client client = new Client(projectId,token,Cloud.ironAWSUSEast);
			//System.out.println("MQ.config "+projectId);
			//System.out.println("MQ.config "+token);

			Queue queue = client.queue(q);
			Log.d("MQ.queue", q);
			queue.push(message);
			Log.d("MQ.message", message);
			} catch (IOException e) {
			e.printStackTrace();
			System.out.println("MQ.error"+e);
			}
		return null;
	}

	@Override
	protected void onPostExecute(JSONObject result) {
	super.onPostExecute(result);

		}


	}
