package tv.loqoo.v4;

import tv.loqoo.v4.R;
import android.content.BroadcastReceiver;
import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.provider.MediaStore;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import com.actionbarsherlock.app.SherlockFragment;

import com.firebase.client.Firebase;
import com.parse.LogInCallback;
import com.parse.ParseException;
import com.parse.ParseTwitterUtils;
import com.parse.ParseUser;


public class FragmentOneWelcome extends SherlockFragment {

	
	private static final String TAG = null;
	public void onCreate(Bundle saved) {
		super.onCreate(saved);
		
		if (null != saved) {
			
			
		}
        Log.i(TAG, "onCreate");
		
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		// Get the view from fragmenttab1.xml
		View view = inflater.inflate(R.layout.fragmenttab1, container, false);
		

		
		return view;
		        
    };
    
    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
    	super.onActivityCreated(savedInstanceState);
    	
    	
    	    	

    }
    
    @Override
	public void onStart() {
		super.onStart();

	}

	@Override
	public void onStop() {
		
		super.onStop();
	}

	
    @Override public void onResume() {
        super.onResume();        
       
    }
    
    @Override public void onPause() {
        super.onPause();        
        
    }
	@Override
	public void onSaveInstanceState(Bundle outState) {
		super.onSaveInstanceState(outState);
		setUserVisibleHint(true);
	}
	
	

}
