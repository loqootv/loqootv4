package tv.loqoo.v4;


import java.sql.Ref;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.actionbarsherlock.app.SherlockFragment;
import com.firebase.client.Firebase;

public class FragmentTwoChanGuide extends SherlockFragment {

	Button addAChannelBtn;
	TextView addAChannel;
	TextView addAChannelStatus;
	EditText addAChannelLabel;
	String aNewChannel;
	Button delAChannelBtn;
	TextView delAChannel;
	TextView delAChannelStatus;
	EditText delAChannelLabel;
	String anOldChannel;
	String networkName;

	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		// Get the view from fragmenttab2.xml
		View view = inflater.inflate(R.layout.fragmenttab2, container, false);
		addAChannelBtn = (Button)view.findViewById(R.id.addAChannelBtn);
		addAChannel = (EditText)view.findViewById(R.id.addAChannel);
		addAChannelStatus = (TextView)view.findViewById(R.id.addAChannelStatus);
		delAChannelBtn = (Button)view.findViewById(R.id.delAChannelBtn);
		delAChannel = (EditText)view.findViewById(R.id.delAChannel);
		delAChannelStatus = (TextView)view.findViewById(R.id.delAChannelStatus);
		
		
		
		return view;
	}
	
	@Override
    public void onActivityCreated(Bundle savedInstanceState) {
    	super.onActivityCreated(savedInstanceState);
    	
    	

    	
    	addAChannelBtn.setOnClickListener(new Button.OnClickListener(){

	    	  @Override
	    	  public void onClick(View arg0) {
	    		  SharedPreferences sharedPrefs = getActivity().getSharedPreferences("networkInfo", 0);
	    	      String projectId = getActivity().getString(R.string.ironmq_project_id);
	    	      String token = getActivity().getString(R.string.ironmq_token);
	    		  networkName = (sharedPrefs.getString("networkName", ""));
	    		  aNewChannel = addAChannel.getText().toString();
	    		  addAChannelStatus.setVisibility(View.INVISIBLE);
	    		  addAChannelStatus.setVisibility(View.VISIBLE);
	    		  addAChannelStatus.setText(aNewChannel+" has been added");
	    		  LoqooApplication application = (LoqooApplication) getActivity().getApplication();
	    		  application.channels.add(aNewChannel);
	    		  application.installation.put("channels", LoqooApplication.channels);
	    		  application.installation.saveInBackground();
	    		  addAChannel.setText("");
	    		  //tell loqootvROBOTS about newly ADDED channel
				  String message = "addedChannel:"+aNewChannel+";"+"networkName:"+networkName+";"+"timestamp:"+System.currentTimeMillis()+";"+"end";
				  IronMQ task = new IronMQ();
				  task.projectId = projectId;
				  task.token = token;
				  task.q = "aChannelAddedToANetwork";
				  task.message = message;
				  task.execute();

				  
	    		  
	    	  }});

	     delAChannelBtn.setOnClickListener(new Button.OnClickListener(){

	    	  @Override
	    	  public void onClick(View arg0) {
	    		  SharedPreferences sharedPrefs = getActivity().getSharedPreferences("networkInfo", 0);
	    	      String projectId = getActivity().getString(R.string.ironmq_project_id);
	    	      String token = getActivity().getString(R.string.ironmq_token);
	    		  anOldChannel = delAChannel.getText().toString();
	    		  delAChannelStatus.setVisibility(View.INVISIBLE);
	    		  delAChannelStatus.setVisibility(View.VISIBLE);
	    		  delAChannelStatus.setText(anOldChannel+" has been removed");
	    		  LoqooApplication application = (LoqooApplication) getActivity().getApplication();
	    		  List<String> r = new LinkedList<String>();
	    		  r.add(anOldChannel);
	    		  application.channels.removeAll(r);
	    		  application.installation.put("channels", LoqooApplication.channels);
	    		  application.installation.saveInBackground();
	    		  delAChannel.setText("");
	    		  // tell loqootvROBOTS about newly REMOVED channel
				  String message = "removedChannel:"+anOldChannel+";"+"networkName:"+networkName+";"+"timeStamp:";
				  IronMQ task = new IronMQ();
				  task.projectId = projectId;
				  task.token = token;
				  task.q = "aChannelRemovedFromANetwork";
				  task.message = message;
				  task.execute();

				  
	    		  
	    	  }});
    }

	@Override
	public void onSaveInstanceState(Bundle outState) {
		super.onSaveInstanceState(outState);
		setUserVisibleHint(true);
	}

}
