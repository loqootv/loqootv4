package tv.loqoo.v4;
 
import tv.loqoo.v4.R;
import android.os.Bundle;
import android.app.Activity;
import android.view.Menu;

import tv.loqoo.v4.LoqooApplication;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.FileInputStream;
import java.io.InputStreamReader;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.HttpURLConnection;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.regex.Pattern;
import java.lang.String;

import android.R.string;
import android.accounts.Account;
import android.accounts.AccountManager;
import android.app.*;
import android.location.Address;
import android.location.Criteria;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.*;
import android.preference.PreferenceManager;
import android.view.*;
import android.webkit.WebChromeClient;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.*;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.content.pm.ActivityInfo;
import android.util.Log;
import android.util.Patterns;

import com.parse.ParseAnalytics;

import com.parse.Parse;
import com.parse.ParseAnalytics;
import com.parse.ParseObject;
import android.graphics.*;

//import com.logentries.android.AndroidLogger;


public class ThirdPartyScene extends Activity {

String substring;
String rstring;
String channelID = "Null";
//public AndroidLogger logger;
public static final String TAG = "LTV";
public static final String TAG2 = "LTVlogger";
public static String mycity = null;
public static String mycountry = null;
public static String mystate = null;
public static String mystreet = null;
public static String myzipcode = null;
public static String myLat = null;
public static String myLong = null;
public static String userLocation = null;
public static String uemail1 = null;
public static String uID = null;	
final Pattern emailPattern = Patterns.EMAIL_ADDRESS;
long Time = System.currentTimeMillis();

    /** Called when the activity is first created. */
    @Override
    public void onCreate(Bundle savedInstanceState)
        {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main);
        setRequestedOrientation(ActivityInfo  
                          .SCREEN_ORIENTATION_PORTRAIT);
                
        
        final ArrayList<String> accountsInfo = new ArrayList<String>();
        final Account[] accounts = AccountManager.get(getBaseContext()).getAccounts();
        for (final Account account : accounts) {
            if (emailPattern.matcher(account.name).matches()) {
                SharedPreferences sharedPrefs = PreferenceManager.getDefaultSharedPreferences(getBaseContext());
                    SharedPreferences.Editor editor = sharedPrefs.edit();
                        editor.putString("uEmail1", account.name.toString());
                        editor.commit();
                uemail1 = (sharedPrefs.getString("uEmail1", ""));
                Log.i(TAG, uemail1);
                // get account name and type
                Log.i(TAG, "account=[" + account.toString() + "]" );
                
                break;
            }
        }

        Intent rIntent = getIntent();
        String rAction = rIntent.getAction();
        String rType = rIntent.getType();
        if (Intent.ACTION_SEND.equals(rAction) && "text/plain".equals(rType) || "image/".equals(rType));
        displaySentText(rIntent);
        

        }

    private void displaySentText (Intent rIntent) {
        
   
                
                
        String contentUrl;
		String title;
        contentUrl = rIntent.getStringExtra(Intent.EXTRA_TEXT).toString();
		title = rIntent.getStringExtra(Intent.EXTRA_TEXT).toString();
		Bitmap bitmap = rIntent.getParcelableExtra("share_screenshot");
        //TextView textMsg = (TextView) findViewById(R.id.URL);
       // textMsg.setText(String.valueOf(contentUrl));
        
        if (contentUrl.startsWith("https://instagram") || contentUrl.startsWith("https://www.instagram")
                        || contentUrl.startsWith("http://instagram") || contentUrl.startsWith("http://www.instagram")) {
						Intent instaIntent = new Intent(ThirdPartyScene.this, SceneInfo.class);
						instaIntent.putExtra("UploadService.ARG_FILE_PATH", contentUrl);
						instaIntent.putExtra("Queue", "new3rdPartyScene");
						startActivity(instaIntent);
        };
        
        if (contentUrl.startsWith("https://soundcloud") || contentUrl.startsWith("https://www.soundcloud")
                || contentUrl.startsWith("http://soundcloud") || contentUrl.startsWith("http://www.soundcloud")) {
			Log.d("loqootv.3rdpartyscene", contentUrl);
        	Intent scIntent = new Intent(ThirdPartyScene.this, SceneInfo.class);
            scIntent.putExtra("UploadService.ARG_FILE_PATH", contentUrl);
            scIntent.putExtra("Queue", "new3rdPartyScene");
		    scIntent.putExtra("SceneTitle", title);
            startActivity(scIntent);
        };
        
        if (contentUrl.startsWith("https://youtube") || contentUrl.startsWith("https://www.youtube") || 
                        contentUrl.startsWith("http://youtube") || contentUrl.startsWith("http://www.youtube")) {
		Log.d("loqootv.3rdpartyscene", contentUrl);
						Intent ytIntent = new Intent(ThirdPartyScene.this, SceneInfo.class);
						ytIntent.putExtra("UploadService.ARG_FILE_PATH", contentUrl);
						ytIntent.putExtra("Queue", "new3rdPartyScene");
						Log.d("loqootv.3rdPartyScene", contentUrl);
						ytIntent.putExtra("SceneTitle", title);
						startActivity(ytIntent);
        
          };
		  
	    
		if (contentUrl.startsWith("https://vine") || contentUrl.startsWith("https://www.vine") || 
			contentUrl.startsWith("http://vine") || contentUrl.startsWith("http://www.vine")) {
			Log.d("loqootv.3rdpartyscene", contentUrl);
			Intent vineIntent = new Intent(ThirdPartyScene.this, SceneInfo.class);
			vineIntent.putExtra("UploadService.ARG_FILE_PATH", contentUrl);
			Log.d("loqootv.3rdPartyScene", contentUrl);  
			vineIntent.putExtra("Queue", "new3rdPartyScene");
			vineIntent.putExtra("SceneTitle", title);
			startActivity(vineIntent);

		};
		
		if (contentUrl.startsWith("https://play.google.com") || contentUrl.startsWith("https://www.play.google.com") || 
			contentUrl.startsWith("http://play.google.com") || contentUrl.startsWith("http://www.play.google.com")) {
			Log.d("loqootv.3rdpartyscene", contentUrl);
			Intent playIntent = new Intent(ThirdPartyScene.this, SceneInfo.class);
			playIntent.putExtra("UploadService.ARG_FILE_PATH", contentUrl);
			playIntent.putExtra("Queue", "new3rdPartyScene");
			playIntent.putExtra("SceneTitle", title);
			playIntent.putExtra("SceneType", "tv.loqoo.v4.THIRDPARTY_PLAY_SCENE");
			Log.d("loqootv.3rdPartyScene", contentUrl);
			startActivity(playIntent);

		};
		
		if (contentUrl.startsWith("https://youtube") || contentUrl.startsWith("https://www.youtube") || 
			contentUrl.startsWith("http://youtube") || contentUrl.startsWith("http://www.youtube")) {
			Log.d("loqootv.3rdpartyscene", contentUrl);
			Intent ytIntent = new Intent(ThirdPartyScene.this, SceneInfo.class);
			ytIntent.putExtra("UploadService.ARG_FILE_PATH", contentUrl);
			ytIntent.putExtra("Queue", "new3rdPartyScene");
			Log.d("loqootv.3rdPartyScene", contentUrl);
			ytIntent.putExtra("SceneTitle", title);
			startActivity(ytIntent);

		};
		
		if (contentUrl.startsWith("https://goo.gl") || contentUrl.startsWith("https://www.goo.gl") || 
			contentUrl.startsWith("http://goo.gl") || contentUrl.startsWith("http://www.goo.gl")) {
			Log.d("loqootv.3rdpartyscene", contentUrl);
			Intent mapsIntent = new Intent(ThirdPartyScene.this, SceneInfo.class);
			mapsIntent.putExtra("UploadService.ARG_FILE_PATH", contentUrl);
			mapsIntent.putExtra("Queue", "new3rdPartyScene");
			Log.d("loqootv.3rdPartyScene", contentUrl);
			mapsIntent.putExtra("SceneTitle", title);
			startActivity(mapsIntent);

		};
    }
  


                public LocationListener listener = new LocationListener() {

                        public void onLocationChanged(Location location) {
                                // TODO Auto-generated method stub
                                double lat = location.getLongitude();
                                double lon = location.getLatitude();
                                
                                String latt = lat+ "";
                                String lonn = lon+ "";
                                
                                Log.d(TAG, latt);
                                Log.d(TAG, lonn);
                                
                             
                           Geocoder gcd = new Geocoder(getBaseContext(),   
                             Locale.getDefault());  

                           List<Address> addresses;  
                           try {  
                                   addresses = gcd.getFromLocation(location.getLatitude(),  
                                              location.getLongitude(), 1);  
                                            if (addresses.size() > 0)
                                                System.out.println(addresses.get(0).getCountryName());  
                                            System.out.println(addresses.get(0).getLocality());
                                            System.out.println(addresses.get(0).getAdminArea());
                                            System.out.println(addresses.get(0).getPostalCode());
                                            System.out.println(addresses.get(0).getThoroughfare());
                                           

                                            String country = addresses.get(0).getCountryName();
                                            String city = addresses.get(0).getLocality();
                                            String state = addresses.get(0).getAdminArea();
                                            String zip = addresses.get(0).getPostalCode();
                                            String street = addresses.get(0).getThoroughfare();
                                            
                                            SharedPreferences sharedPrefs = PreferenceManager.getDefaultSharedPreferences(getBaseContext());
                                            SharedPreferences.Editor editor = sharedPrefs.edit();
                                                editor.putString("UsersCountry", country);
                                                editor.putString("UsersCity", city);
                                                editor.putString("UsersState", state);
                                                editor.putString("UsersZip", zip);
                                                editor.putString("UsersStreet", street);
                                                editor.commit();
                                        mycountry = (sharedPrefs.getString("UsersCountry", ""));
                                        mycity = (sharedPrefs.getString("UsersCity", ""));
                                        mystate = (sharedPrefs.getString("UsersState", ""));
                                        myzipcode = (sharedPrefs.getString("UsersZip", ""));
                                        mystreet = (sharedPrefs.getString("UsersStreet", ""));
                                        
                                        String userLocation = " country:"+mycountry+" city:"+mycity +" state:"+mystate +" street:"+mystreet;
                                        Log.d(TAG2, userLocation);
                                                
                                           } catch (IOException e) {  
                                            e.printStackTrace();  
                                           }
                                                }
                           
                        
                        public void onProviderDisabled(String provider) {
                                // TODO Auto-generated method stub
                                
                        }

                        public void onProviderEnabled(String provider) {
                                // TODO Auto-generated method stub
                                
                        }

                        public void onStatusChanged(String provider, int status,
                                        Bundle extras) {
                                // TODO Auto-generated method stub
                                
                        }
                                                
            };
            
            
              
        
        @Override
        public void onStart() {
          super.onStart();
          
        }

        @Override
        public void onStop() {
          super.onStop();
         
        }
            

            
}
