package tv.loqoo.v4;


import tv.loqoo.v4.R;
import tv.loqoo.v4.Join;


import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.ViewPager;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;

import com.navdrawer.SimpleSideDrawer;
import com.parse.ParseUser;

import com.actionbarsherlock.app.ActionBar;
import com.actionbarsherlock.app.ActionBar.Tab;
import com.actionbarsherlock.app.SherlockFragmentActivity;
import com.actionbarsherlock.view.Menu;
import com.actionbarsherlock.view.MenuInflater;
import com.actionbarsherlock.view.MenuItem;

public class MyChannels extends SherlockFragmentActivity {

	// Declare Variables
	ActionBar mActionBar;
	ViewPager mPager;
	Tab tab;
	
	private SimpleSideDrawer LeftSliderMenu;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		// Get the view from activity_main.xml
		setContentView(R.layout.mychannel_activity);
		if (getResources().getBoolean(R.bool.portrait_only)) {
			setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
		}
		LeftSliderMenu = new SimpleSideDrawer(this);
        LeftSliderMenu.setLeftBehindContentView(R.layout.left_slider_menu);
		
		// Activate Navigation Mode Tabs
		mActionBar = getSupportActionBar();
		mActionBar.setDisplayHomeAsUpEnabled(true);
		mActionBar.setHomeButtonEnabled(true);
		mActionBar.setNavigationMode(ActionBar.NAVIGATION_MODE_TABS);
		
		// Locate ViewPager in activity_main.xml
		mPager = (ViewPager) findViewById(R.id.mychannel_pager);
		
		// Activate Fragment Manager
		FragmentManager fm = getSupportFragmentManager();

		// Capture ViewPager page swipes
		ViewPager.SimpleOnPageChangeListener ViewPagerListener = new ViewPager.SimpleOnPageChangeListener() {
			@Override
			public void onPageSelected(int position) {
				super.onPageSelected(position);
				// Find the ViewPager Position
				mActionBar.setSelectedNavigationItem(position);
			}
		};

		mPager.setOnPageChangeListener(ViewPagerListener);
		// Locate the adapter class called ViewPagerAdapter.java
		MyChannelViewPagerAdapter viewpageradapter = new MyChannelViewPagerAdapter(fm);
		// Set the View Pager Adapter into ViewPager
		mPager.setAdapter(viewpageradapter);
		
		// Capture tab button clicks
		ActionBar.TabListener tabListener = new ActionBar.TabListener() {

			@Override
			public void onTabSelected(Tab tab, FragmentTransaction ft) {
				// Pass the position on tab click to ViewPager
				mPager.setCurrentItem(tab.getPosition());
			}

			@Override
			public void onTabUnselected(Tab tab, FragmentTransaction ft) {
				// TODO Auto-generated method stub
			}

			@Override
			public void onTabReselected(Tab tab, FragmentTransaction ft) {
				// TODO Auto-generated method stub
			}
		};
		
		// Create first Tab
		tab = mActionBar.newTab().setText("new").setTabListener(tabListener);
		mActionBar.addTab(tab);
		
		// Create second Tab
		tab = mActionBar.newTab().setText("1").setTabListener(tabListener);
		mActionBar.addTab(tab);
		
		// Create third Tab
		tab = mActionBar.newTab().setText("2").setTabListener(tabListener);
		mActionBar.addTab(tab);
		
		tab = mActionBar.newTab().setText("3").setTabListener(tabListener);
		mActionBar.addTab(tab);

	}
	
	@Override
		public boolean onCreateOptionsMenu(Menu menu) {
			com.actionbarsherlock.view.MenuInflater inflater = getSupportMenuInflater();
			inflater.inflate(R.menu.main, (com.actionbarsherlock.view.Menu) menu);
			return super.onCreateOptionsMenu(menu);
	}
	
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		case android.R.id.home:
				LeftSliderMenu.toggleLeftDrawer();
			}
		return super.onOptionsItemSelected(item);
		}
	
	@Override
	public boolean onPrepareOptionsMenu(Menu menu) {
		MenuItem actionViewItem = menu.findItem(R.id.av);
		View v = actionViewItem.getActionView();
		ImageButton addSceneNotesBtn = (ImageButton) v.findViewById(R.id.addSceneNotes_btn);
		ImageButton addSceneAVBtn = (ImageButton) v.findViewById(R.id.addSceneAV_btn);

		addSceneNotesBtn.setOnClickListener(new Button.OnClickListener(){

	    	  @Override
	    	  public void onClick(View arg0) {
	    		  
	    		  Intent intent = new Intent(MyChannels.this, DirectSceneNote.class);
	              startActivity(intent);
	    		  
	    	  }	
	     }); 
		addSceneAVBtn.setOnClickListener(new Button.OnClickListener(){

	    	  @Override
	    	  public void onClick(View arg0) {
	    		  
	    		  Intent intent = new Intent(MyChannels.this, DirectSceneAV.class);
	              startActivity(intent);
	    		  
	    	  }	
	     });
		return super.onPrepareOptionsMenu(menu);
	}

    @Override public void onResume() {
        super.onResume();        

    }
    
    @Override public void onPause() {
        super.onPause();        

    }
	

}


