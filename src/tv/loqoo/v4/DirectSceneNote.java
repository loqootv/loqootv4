package tv.loqoo.v4;

import java.util.ArrayList;

import com.aviary.android.feather.FeatherActivity;
import com.aviary.android.feather.library.Constants;

import tv.loqoo.v4.R;
import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.ActivityInfo;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.TextView;

public class DirectSceneNote extends Activity {

	private static final int FILE_SELECT_CODE = 0;
	
	Button select;
	Button interrupt;
	ProgressBar progress;
	TextView status;

	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.directscene_av);
		if (getResources().getBoolean(R.bool.portrait_only)) {
			setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
		}
		
		select = (Button) findViewById(R.id.btn_select);
		interrupt = (Button) findViewById(R.id.btn_interrupt);
		progress = (ProgressBar) findViewById(R.id.progress);
		status = (TextView) findViewById(R.id.status);

		
		
		
		select.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				// start file chooser
				Intent intent = new Intent( DirectSceneNote.this, FeatherActivity.class );
				intent.setData( Uri.parse("https://loqootvstar.s3.amazonaws.com/blanknote.jpg"));
				intent.putExtra(Constants.EXTRA_EFFECTS_ENABLE_FAST_PREVIEW, true);
				intent.putExtra(Constants.EXTRA_IN_SAVE_ON_NO_CHANGES, false);

				intent.putExtra(Constants.EXTRA_TOOLS_LIST, new String[] {
						"STICKERS", "TEXT", "MEME", "DRAWING", "EFFECTS", "CROP"});
				startActivityForResult( intent, 1);
				
			}
		});
		
		interrupt.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				// interrupt any active upload
				Intent intent = new Intent(UploadService.UPLOAD_CANCELLED_ACTION);
				sendBroadcast(intent);
			}
		});
		
	}
	
	@Override
	protected void onStart() {
		super.onStart();
		IntentFilter f = new IntentFilter();
		f.addAction(UploadService.UPLOAD_STATE_CHANGED_ACTION);
		registerReceiver(uploadStateReceiver, f);
	}

	@Override
	protected void onStop() {
		unregisterReceiver(uploadStateReceiver);
		super.onStop();
	}
	
	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
			if (resultCode == RESULT_OK) {
				switch( requestCode) {
					case 1:
						// get path of selected file 
						Uri uri = data.getData();
						Bundle extra = data.getExtras();
						String path = getPathFromContentUri(uri);
						Log.d("S3", "uri=" + uri.toString());
						Log.d("S3", "path=" + path);
						// initiate the upload
						Intent intent = new Intent(this, UploadService.class);
						intent.putExtra(UploadService.ARG_FILE_PATH, path);
						startService(intent);
						break;
			}
		}
		super.onActivityResult(requestCode, resultCode, data);
	}
	
	private String getPathFromContentUri(Uri uri) {
		String path = uri.getPath();
		if (uri.toString().startsWith("content://")) {
			String[] projection = { MediaStore.MediaColumns.DATA };
			ContentResolver cr = getApplicationContext().getContentResolver();
			Cursor cursor = cr.query(uri, projection, null, null, null);
			if (cursor != null) {
				try {
					if (cursor.moveToFirst()) {
						path = cursor.getString(0);
					}
				} finally {
					cursor.close();
				}
			}

		}
		return path;
	}
	 
	private BroadcastReceiver uploadStateReceiver = new BroadcastReceiver() {

		@Override
        public void onReceive(Context context, Intent intent) {
        	Bundle b = intent.getExtras();
        	status.setText(b.getString(UploadService.MSG_EXTRA));
        	//Log.d("s3Url", b.getString(UploadService.MSG_EXTRA));
        	//myImage.setImageUrl(b.getString(UploadService.MSG_EXTRA));
        	int percent = b.getInt(UploadService.PERCENT_EXTRA);
        	progress.setIndeterminate(percent < 0);
        	progress.setProgress(percent);
        }
    };
    

    

}
