package tv.loqoo.v4;


import tv.loqoo.v4.R;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.os.Bundle;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Handler;
import android.preference.PreferenceManager;
import android.view.animation.AnimationUtils;
import android.view.animation.Animation;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.ViewFlipper;
import android.widget.Button;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.parse.Parse;
import com.parse.ParseAnalytics;
import com.parse.ParseUser;

public class Splash extends Activity {
	

@Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.splash);
        ParseAnalytics.trackAppOpened(getIntent());
        final ViewFlipper SplashFlipper = (ViewFlipper)findViewById(R.id.splash_flipper);
        
  	   
       Animation animationFlipIn  = AnimationUtils.loadAnimation(this, R.layout.fadein);
       Animation animationFlipOut = AnimationUtils.loadAnimation(this, R.layout.fadeout);
       SplashFlipper.setInAnimation(animationFlipIn);
       SplashFlipper.setOutAnimation(animationFlipOut);
       SplashFlipper.setFlipInterval(LoqooApplication.SPLASH_TRANSITION_INTERVAL);
       final Button buttonAutoFlip = (Button)findViewById(R.id.test);
       final ImageButton buttonJoin = (ImageButton)findViewById(R.id.join);
       final ImageButton buttonLogon = (ImageButton)findViewById(R.id.logon);
       buttonAutoFlip.setOnClickListener(new Button.OnClickListener(){
    
    	  @Override
    	  public void onClick(View arg0) {
    		  // TODO Auto-generated method stub
    
    		  if(SplashFlipper.isFlipping()){
    			  SplashFlipper.stopFlipping();
    			  buttonAutoFlip.setText("Start Auto Flip");
    		  }else{
    			  SplashFlipper.startFlipping();
    			  buttonAutoFlip.setText("Stop Auto Flip");
    		  }
    	  	}});

buttonAutoFlip.performClick();

	  buttonJoin.setOnClickListener(new Button.OnClickListener(){

		  @Override
		  public void onClick(View arg0) {
			  
			  SharedPreferences sharedPrefs = getSharedPreferences("networkInfo", 0);
		  	  String networkName = (sharedPrefs.getString("networkName", ""));
			  ParseUser currentUser = ParseUser.getCurrentUser();
    		  //String channel = currentUser.getUsername().toUpperCase();
    		  if(currentUser != null) {
    			  customToast("Your Already Signed Up..." +
    			  		"Welcome Back #LOQOOTVx"+networkName.toUpperCase());
                Intent intent = new Intent(Splash.this, MainActivity.class);
                startActivity(intent);
    		  } else{
    			  Intent intent = new Intent(Splash.this, Join.class);
                  startActivity(intent);
    		  }
		  }});

	  buttonLogon.setOnClickListener(new Button.OnClickListener(){

		  @SuppressWarnings("unused")
		@Override
		  public void onClick(View arg0) {
			  
			  SharedPreferences sharedPrefs = getSharedPreferences("networkInfo", 0);		  	  String networkName = (sharedPrefs.getString("networkName", ""));
    		  ParseUser currentUser = ParseUser.getCurrentUser();
    		  //String channel = currentUser.getUsername();
    		  if(currentUser != null) {
    			  customToast("Welcome Back #LOQOOTVx"+networkName.toUpperCase());
    			  userLoggedInAlready();
    		  } else{
                  Intent intent = new Intent(Splash.this, Logon.class);
                  startActivity(intent);
    		  }

		  }});
	  

	  
	  
  //  Boolean splashHandler = new Handler().postDelayed(new Thread() {
   //          @SuppressLint("NewApi")
	//		@Override
    //        public void run() {
            	    
     //               Intent mainMenu = new Intent(Splash.this,
       //             		MainActivity.class);
      //              Splash.this.startActivity(mainMenu);
        //            Splash.this.finish();
        //            overridePendingTransition(R.layout.fadein,R.layout.fadeout);
      //   }
  //   }, LoqooApplication.MAIN_THREAD_DELAY);

  }

		public void userLoggedInAlready() {
        Intent intent = new Intent(Splash.this, MainActivity.class);
        startActivity(intent);		
	};
	
	 private void customToast(String message) {
		 LayoutInflater li = getLayoutInflater();
		 View toastLayout = li.inflate(R.layout.toast, (ViewGroup)findViewById
				 (R.id.toastLayout));
		 
	 	 TextView text = (TextView) toastLayout.findViewById(R.id.toastText);
	 	 text.setText(message);
	 	 Toast toast = new Toast(this);
	 	 toast.setDuration(Toast.LENGTH_LONG);
	 	 toast.setView(toastLayout);
	 	 toast.show();
	 }



}