package tv.loqoo.v4;


import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

public class MyChannellVPAdapter extends FragmentPagerAdapter {

	// Declare the number of ViewPager pages
	final int PAGE_COUNT = 4;

	public MyChannellVPAdapter(FragmentManager fm) {
		super(fm);
	}

	@Override
	public Fragment getItem(int arg0) {
		switch (arg0) {

			// Open FragmentTab1.java
		case 0:
			ChannelOne fragmenttab5 = new ChannelOne();
			return fragmenttab5;

			// Open FragmentTab2.java
		case 1:
			ChannelTwo fragmenttab6 = new ChannelTwo();
			return fragmenttab6;

			// Open FragmentTab3.java
		case 2:
			ChannelOne fragmenttab7 = new ChannelOne();
			return fragmenttab7;
			
		case 3:
			ChannelTwo fragmenttab8 = new ChannelTwo();
			return fragmenttab8;
		}
		return null;
	}

	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		return PAGE_COUNT;
	}

}


