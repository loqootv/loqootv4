package tv.loqoo.v4;


import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

public class ViewPagerAdapter extends FragmentPagerAdapter {

	// Declare the number of ViewPager pages
	final int PAGE_COUNT = 4;

	public ViewPagerAdapter(FragmentManager fm) {
		super(fm);
	}

	@Override
	public Fragment getItem(int arg0) {
		switch (arg0) {

			// Open FragmentTab1.java
		case 0:
			FragmentOneWelcome fragmenttab1 = new FragmentOneWelcome();
			return fragmenttab1;

			// Open FragmentTab2.java
		case 1:
			FragmentTwoChanGuide fragmenttab2 = new FragmentTwoChanGuide();
			return fragmenttab2;

			// Open FragmentTab3.java
		case 2:
			ChannelOne fragmenttab3 = new ChannelOne();
			return fragmenttab3;
			
		case 3:
			ChannelTwo fragmenttab4 = new ChannelTwo();
			return fragmenttab4;
		}
		return null;
	}

	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		return PAGE_COUNT;
	}

}


