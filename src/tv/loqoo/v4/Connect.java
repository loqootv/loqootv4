package tv.loqoo.v4;


import com.parse.LogInCallback;
import com.parse.ParseException;
import com.parse.ParseTwitterUtils;
import com.parse.ParseUser;
import com.parse.SaveCallback;

import tv.loqoo.v4.R;
import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.TextView;



public class Connect extends Activity {
	
	ImageButton twitterBtn;
	ImageButton facebookBtn;
	TextView test;

	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.connect);
		
		//test = (TextView)findViewById(R.id.test);
		//twitterBtn = (ImageButton)findViewById(R.id.twitter);
		//facebookBtn = (ImageButton)findViewById(R.id.facebook);
		
		
		twitterBtn.setOnClickListener(new Button.OnClickListener(){

			 public void onClick(View arg0) {
				 final ParseUser user = ParseUser.getCurrentUser();
				 if (!ParseTwitterUtils.isLinked(user)) {
					 ParseTwitterUtils.link(user, Connect.this, new SaveCallback() {
						 @Override
						 public void done(ParseException ex) {
							 if (ParseTwitterUtils.isLinked(user)) {
								 
							 }
						 }
					 });
				 }
		
		}});
		
		
		facebookBtn.setOnClickListener(new Button.OnClickListener(){	
			 public void onClick(View arg0) {
		 ParseTwitterUtils.logIn(Connect.this, new LogInCallback() {
			  public void done(ParseUser user, ParseException err) {
				  if (user == null) {
						  Log.d("MyApp", "cancelled");
				  } else if (user.isNew()) {
					  	  Log.d("MyApp", "signup and logged in");
				  } else {
					  	  Log.d("MyApp", "logged in");
				  }
			  }
		  });
				
	}});
	
		
	}
}
	
	

