package tv.loqoo.v4;


import tv.loqoo.v4.R;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.ActivityInfo;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.provider.Settings;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.parse.LogInCallback;
import com.parse.ParseException;
import com.parse.ParseInstallation;
import com.parse.ParseUser;
import com.parse.SaveCallback;
import com.parse.SignUpCallback;


public class Logon extends Activity {
	
	EditText networkName;
	EditText networkPassword;
	EditText networkTwScreename;
	EditText networkFbScreename;
	ImageView logo;
	ImageButton buttonLogon;
	ProgressBar loggingInProgressBar;
	TextView loggingInLabel;
	String username;
	String password;
	TextView logOnRequiredText;
	TextView logOnText;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		if (getResources().getBoolean(R.bool.portrait_only)) {
			setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
		}
		setContentView(R.layout.logon);
		
		networkName = (EditText)findViewById(R.id.networkName);
		networkPassword = (EditText)findViewById(R.id.networkPassword);
		logo = (ImageView)findViewById(R.id.ltvIconLogOn);
		buttonLogon = (ImageButton)findViewById(R.id.ready);
		loggingInLabel = (TextView)findViewById(R.id.loggingInLabel);
		loggingInProgressBar = (ProgressBar)findViewById(R.id.loggingInProgressBar);
		logOnText = (TextView)findViewById(R.id.logOnText);
		logOnRequiredText = (TextView)findViewById(R.id.requireLOGONText);
		//password = channelPassword.getText().toString();
		//username = channelName.getText().toString();

	     buttonLogon.setOnClickListener(new Button.OnClickListener(){

	    	  @Override
	    	  public void onClick(View arg0) {
	    		  
	    			logOnRequiredText.setVisibility(View.INVISIBLE);
	    			logOnText.setVisibility(View.INVISIBLE);
	    			loggingInLabel.setVisibility(View.VISIBLE);
	    			loggingInProgressBar.setVisibility(View.VISIBLE);
	    			logo.setVisibility(View.VISIBLE);

	    		  password = networkPassword.getText().toString();
	    		  username = networkName.getText().toString();
	    		  
	    		  ParseUser.logInInBackground(username, password, new LogInCallback() {
	    			  
	    			  public void done (ParseUser user, ParseException e) {
	    				  customToast("TV is DEAD, but LOQOOTV is SO ALIVE!");
	    				  if(user != null) {
	    					  
							  SharedPreferences sharedPrefs = getSharedPreferences("networkInfo", 0);
	    	                  SharedPreferences.Editor editor = sharedPrefs.edit();
	    	                  editor.putString("networkName", username);
	    	                  editor.putString("networkEmail", user.getEmail());
	    	                  editor.commit();
	    	                  LoqooApplication application = (LoqooApplication) Logon.this.getApplication();
	    	          		  application.channels.add(username);
	    	          		  application.installation.put("channels", LoqooApplication.channels);
	    	          		  application.installation.saveInBackground();
							  String projectId = getString(R.string.ironmq_project_id);
							  String token = getString(R.string.ironmq_token);
							  String message = username;
							  IronMQ task = new IronMQ();
							  task.projectId = projectId;
							  task.token = token;
							  task.q = "justLoggedIn";
							  task.message = message;
							  task.execute();
							  
	    	                  String networkName = (sharedPrefs.getString("networkName", ""));
	    					  customToast(networkName.toUpperCase()+" Network is Now On #LOQOOTV");
	    					  Intent intent = new Intent(Logon.this, MainActivity.class);
	    				        startActivity(intent);	
	    				  } else{
	    					  logInNoGood(e);
	    				  }
	    			  }
	    		  
	    		  });

    	  }});
        
	}
	
	public void logInNoGood(ParseException e) {
		loggingInLabel.setVisibility(View.INVISIBLE);
		loggingInProgressBar.setVisibility(View.INVISIBLE);
		logo.setVisibility(View.INVISIBLE);
		String err = e.toString();
		customToast(err);
		customToast("Please Fix That");
		  String projectId = getString(R.string.ironmq_project_id);
		  String token = getString(R.string.ironmq_token);
		  String message = username;
		  IronMQ task = new IronMQ();
		  task.projectId = projectId;
		  task.token = token;
		  task.q = "logInBad";
		  task.message = message;
		  task.execute();
		
	}
	
	protected void onStart(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		
		
	}
	
	 private Boolean isOnline() {
		  ConnectivityManager cm = 
				  (ConnectivityManager)getSystemService(Context.CONNECTIVITY_SERVICE);
		  NetworkInfo ni = cm.getActiveNetworkInfo();
		  if(ni != null && ni.isConnected())
			  return true;
			  
		  return false;
	 }
	 
	 private void customToast(String message) {
		 LayoutInflater li = getLayoutInflater();
		 View toastLayout = li.inflate(R.layout.toast, (ViewGroup)findViewById
				 (R.id.toastLayout));
		 
	 	 TextView text = (TextView) toastLayout.findViewById(R.id.toastText);
	 	 text.setText(message);
	 	 Toast toast = new Toast(this);
	 	 toast.setDuration(Toast.LENGTH_LONG);
	 	 toast.setView(toastLayout);
	 	 toast.show();
	 }
	 

}
