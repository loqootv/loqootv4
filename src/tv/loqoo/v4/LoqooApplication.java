
package tv.loqoo.v4;


import java.io.IOException;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;

import com.parse.Parse;
import com.parse.ParseACL;
import com.parse.ParseTwitterUtils;
import com.parse.PushService;
import com.parse.ParseInstallation;
import com.parse.ParseUser;

import android.app.Application;
import android.provider.Settings;
import android.util.Log;
import android.view.View;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;

import com.firebase.client.DataSnapshot;
import com.firebase.client.Firebase;
import com.firebase.client.ValueEventListener;
import android.content.*;
import android.preference.*;

public class LoqooApplication extends Application {
	
    public static final int SPLASH_TRANSITION_INTERVAL = 7000;
    public static final int MAIN_THREAD_DELAY = 10000;
	public static ParseInstallation installation;
	public static List<String> channels = new LinkedList<String>();

	@Override
	public void onCreate() {
		super.onCreate();
		
		try {
			Class.forName("android.os.AsyncTask");
		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		// Add your initialization code here

		Parse.initialize(this, "fBzdHC1OLCMBmOCvzBLAplhvTtT9BvlJepf3qbab", "mWGL6wJ9EEiJwIvBDGH265rsxbspnUl9qdhyarr6");
		ParseTwitterUtils.initialize("pTasxTfUYd3Cn7KZI9qjg", "OcdNq17PYDWTA6NtxPjUKKEZxVM2tgFqg8XaSZnVs");
		installation.getCurrentInstallation().saveInBackground();
		String androidID = Settings.Secure.getString(getContentResolver(),Settings.Secure.ANDROID_ID);
		Log.v("LoqooApplication.androidID", androidID);
		installation = ParseInstallation.getCurrentInstallation();
		installation.put("androidID", androidID);
		channels.add("loqootv");
		installation.put("channels", channels);
		//addChannelsListToPreferences();
		Log.v("TAG","OK");		
		PushService.setDefaultPushCallback(this, Splash.class);
		//Log.v("ParseInstallation","saveInBackground");
		//ParseUser.enableAutomaticUser();
		ParseACL defaultACL = new ParseACL();	
		ParseACL.setDefaultACL(defaultACL, true);
	}

	public void subscribeToChannel(Context context, String channel) {
		PushService.subscribe(context, channel, MainActivity.class);
	}
	
	public void unsubscribeFromChannel(Context context, String channel) {
		PushService.unsubscribe(context, channel);
	}	
	
	public void LogOut(final View view) {
		ParseUser.logOut();
		SharedPreferences sharedPrefs = PreferenceManager.getDefaultSharedPreferences(getBaseContext());
		String networkName = (sharedPrefs.getString("networkName", ""));
		String networkEmail = (sharedPrefs.getString("networkEmail", ""));
		String projectId = getString(R.string.ironmq_project_id);
		String token = getString(R.string.ironmq_token);
		String message = networkName+":"+networkEmail;
		IronMQ task = new IronMQ();
		task.projectId = projectId;
		task.token = token;
		task.q = "justLoggedOut";
		task.message = message;
		task.execute();
		
		Intent intent = new Intent(LoqooApplication.this, Splash.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_MULTIPLE_TASK);
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
		startActivity(intent);
	}
	
	public void MyChannels(final View view) {
		ParseUser.logOut();
		Intent intent = new Intent(LoqooApplication.this, MyChannels.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_MULTIPLE_TASK);
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
		startActivity(intent);
	}
	
	public void Home(final View view) {
		ParseUser.logOut();
		Intent intent = new Intent(LoqooApplication.this, MainActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_MULTIPLE_TASK);
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
		startActivity(intent);
	}

	public void MyBank(final View view) {
		ParseUser.logOut();
		Intent intent = new Intent(LoqooApplication.this, MyBank.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_MULTIPLE_TASK);
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
		startActivity(intent);
	}
	
	public void MyInfo(final View view) {
		ParseUser.logOut();
		Intent intent = new Intent(LoqooApplication.this, MyInfo.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_MULTIPLE_TASK);
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
		startActivity(intent);
	}
	
	
}
