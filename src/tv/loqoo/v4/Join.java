package tv.loqoo.v4;


import java.util.LinkedList;
import java.util.List;
import java.util.Locale;

import tv.loqoo.v4.R;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.ActivityInfo;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.provider.Settings;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

import com.parse.LogInCallback;
import com.parse.ParseException;
import com.parse.ParseGeoPoint;
import com.parse.ParseInstallation;
import com.parse.ParseUser;
import com.parse.SaveCallback;
import com.parse.SignUpCallback;
import android.content.Context;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;

public class Join extends Activity implements LocationListener {
	
	protected LocationManager locationManager;
	protected LocationListener locationListener;
	protected Context context;
	EditText networkName;
	EditText networkPassword;
	EditText networkEmail;
	ImageButton buttonJoin;
	String username;
	String password;
	String email;
	String nsqPath = "http://1.gui.loqoo.tv:4161/create_topic?topic=";
	HttpGet get = new HttpGet();
	public static LoqooApplication application;
	ParseUser user = new ParseUser();
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.join_who);
		if (getResources().getBoolean(R.bool.portrait_only)) {
			setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
		}
		
		networkName = (EditText)findViewById(R.id.networkName);
		networkPassword = (EditText)findViewById(R.id.networkPassword);
		networkEmail = (EditText)findViewById(R.id.networkEmail);
		buttonJoin = (ImageButton)findViewById(R.id.ready);
		
		

		//password = networkPassword.getText().toString();
		 username = networkName.getText().toString();
		 password = networkPassword.getText().toString();
		 email = networkEmail.getText().toString();
	     buttonJoin.setOnClickListener(new Button.OnClickListener(){

	    	  @Override
	    	  public void onClick(View arg0) {
	    		  
	    		  if(isOnline() == false){
	    			  customToast("LOQOOTV! is off the Internets, We needs the Internets pls" +
	    			  		"Connection");
	    			  return;
	    		  }else{
	    		 
	    		 
	    		  username = networkName.getText().toString();
	    		  email = networkEmail.getText().toString();
	    		  password = networkPassword.getText().toString();
	    		  
	    		  if (username != null) {
	    			  user.setUsername(username);

	    		  }else{Log.v("TAG", "user is blank");
	    		  		
	    		  }if (password != null) {
	    			  user.setPassword(password);

	    		  }else{ 
	    			  Log.v("TAG", "pass is blank");
	    		  }if (email != null) {
	    			  user.setEmail(email);

	    		  }else{ 
	    			  Log.v("TAG", "email is blank");
	    		  };
	    		  
	    		 customToast(" Your Whole Life is About" +
	    		 		" To Change, Give Us A Sec Tho :).");
	    		  
	    		 user.signUpInBackground(new SignUpCallback() {
	    				public void done(ParseException e) {
	    					if (e == null) {
	    						signUpGood();
	    					} else{
	    						 Log.v("TAG.", "ERROR, nah man can't sign the user up bro");
	    						 signUpBad(e);
	    					}
	    			}});
	    		  }
    	  }});
	}  
		  
	public void signUpGood(){
		
		getGeo();
		username = networkName.getText().toString();
		email = networkEmail.getText().toString();
		SharedPreferences sharedPrefs = getSharedPreferences("networkInfo", 0);
		SharedPreferences.Editor editor = sharedPrefs.edit();
		editor.clear();
		editor.commit();
        editor.putString("networkName", username);
		editor.putString("networkEmail", email);
        editor.commit();
		
		String go = nsqPath+username;
		Log.v("signUpGood.nsqCHAN", go);
		get.execute(go);
					
		customToast(username.toUpperCase(Locale.getDefault())+
				" Welcome to LOQOOTV...just remember, U are TV!");
				
		String projectId = getString(R.string.ironmq_project_id);
		String token = getString(R.string.ironmq_token);
		String message = username+":"+email;
		IronMQ task = new IronMQ();
		task.projectId = projectId;
		task.token = token;
		task.q = "newNetworkSignup";
		task.message = message;
		task.execute();
		//Give each network it's own Queue
		String message1 = username+":"+email;
		IronMQ task1 = new IronMQ();
		task1.projectId = projectId;
		task1.token = token;
		task1.q = "/network/"+username;
		task1.message = message1;
		task1.execute();
		
		
		ParseUser.logInInBackground(username, password, new LogInCallback() {
			public void done(ParseUser user, ParseException e) {
				if(user != null) {
					 Log.v("TAG", "OK, all logged up, time for the new user to go to the welcome screen");
					//Now take this newly created network to welcome screen
					 newUserLogInGood();
				} else{
	    			 Log.v("TAG", "ERROR, nah man can't log the user in");
	    			 newUserLogInBad(e);
				}
			}
		});
	}
	
	public void signUpBad(ParseException e){
		String err = e.toString();
		String projectId = getString(R.string.ironmq_project_id);
		String token = getString(R.string.ironmq_token);
		String message = username+":"+email+":"+e.toString();
		IronMQ task = new IronMQ();
		task.projectId = projectId;
		task.token = token;
		task.q = "newSignUpBad";
		task.message = message;
		task.execute();
		customToast(err+" Also, Make Sure " +
				"the Correct Date and Time is Set");
		Log.d("ERROR.signupbad", e.toString());
		
	}
	
	public void newUserLogInGood(){
		application = (LoqooApplication) this.getApplication();
		getGeo();
		LoqooApplication.channels.add(username);
		LoqooApplication.installation.put("channels", LoqooApplication.channels);
		LoqooApplication.installation.saveInBackground();
		//application.networks.add("three");
		//application.installation.put("channels",LoqooApplication.channels);
		//application.installation.saveInBackground();
		//send justlogged in msg
		String projectId = getString(R.string.ironmq_project_id);
		String token = getString(R.string.ironmq_token);
		String message = username+":"+email+"eventType:"+"login";
		IronMQ task = new IronMQ();
		task.projectId = projectId;
		task.token = token;
		task.q = "justLoggedin";
		task.message = message;
		task.execute();
		//send added networks msg
		String message1 = username+":"+email;
		IronMQ task1 = new IronMQ();
		task1.projectId = projectId;
		task1.token = token;
		task1.q = "newChannelAdded";
		task1.message = message1;
		task1	.execute();
		
        	Intent welcome = new Intent(Join.this,
        	MainActivity.class);
        	Join.this.startActivity(welcome);
        	Join.this.finish();

	}
	

	public void newUserLogInBad(ParseException e){
		String err = e.toString();
		customToast(err);
		Log.d("ERROR.newuserloginbad", e.toString());
	}
	
	 private Boolean isOnline() {
		  ConnectivityManager cm = 
				  (ConnectivityManager)getSystemService(Context.CONNECTIVITY_SERVICE);
		  NetworkInfo ni = cm.getActiveNetworkInfo();
		  if(ni != null && ni.isConnected())
			  return true;
			  
		  return false;
	 }
	 
	 private void customToast(String message) {
		 LayoutInflater li = getLayoutInflater();
		 View toastLayout = li.inflate(R.layout.toast, (ViewGroup)findViewById
				 (R.id.toastLayout));
		 
	 	 TextView text = (TextView) toastLayout.findViewById(R.id.toastText);
	 	 text.setText(message);
	 	 Toast toast = new Toast(this);
	 	 toast.setDuration(Toast.LENGTH_LONG);
	 	 toast.setView(toastLayout);
	 	 toast.show();
	 }
	 
	 public void getGeo() {
		locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
		locationManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, 0, 0, this);
		Log.d("LOQOOTVgeolocation", "working....");
	 }
	 
		@Override
		public void onLocationChanged(Location location) {
			double Latitude = location.getLatitude();
			double Longitude = location.getLongitude();
			ParseGeoPoint point = new ParseGeoPoint(Latitude, Longitude);
			user.put("location", point);
			System.out.printf("LOQOOTVgeolocation", point);
		}
		 
		@Override
		public void onProviderDisabled(String provider) {
		Log.d("Latitude","disable");
		}
		 
		@Override
		public void onProviderEnabled(String provider) {
		Log.d("Latitude","enable");
		}
		 
		@Override
		public void onStatusChanged(String provider, int status, Bundle extras) {
		Log.d("Latitude","status");
		}
		
		


}
