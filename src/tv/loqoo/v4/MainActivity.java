package tv.loqoo.v4;


import tv.loqoo.v4.R;
import tv.loqoo.v4.Join;


import android.content.ContentResolver;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.ViewPager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

import com.navdrawer.SimpleSideDrawer;
import com.parse.ParseUser;

import com.actionbarsherlock.app.ActionBar;
import com.actionbarsherlock.app.ActionBar.Tab;
import com.actionbarsherlock.app.SherlockFragmentActivity;
import com.actionbarsherlock.view.Menu;
import com.actionbarsherlock.view.MenuInflater;
import com.actionbarsherlock.view.MenuItem;

public class MainActivity extends SherlockFragmentActivity {


	ActionBar mActionBar;
	ViewPager mPager;
	Tab tab;
	private static final int FILE_SELECT_CODE = 0;
	
	private SimpleSideDrawer LeftSliderMenu;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		// Get the view from activity_main.xml
		setContentView(R.layout.activity_main);
		if (getResources().getBoolean(R.bool.portrait_only)) {
			setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
		}
		LeftSliderMenu = new SimpleSideDrawer(this);
        LeftSliderMenu.setLeftBehindContentView(R.layout.left_slider_menu);
		
		// Activate Navigation Mode Tabs
		mActionBar = getSupportActionBar();
		mActionBar.setDisplayHomeAsUpEnabled(true);
		mActionBar.setHomeButtonEnabled(true);
		mActionBar.setNavigationMode(ActionBar.NAVIGATION_MODE_TABS);
		
		// Locate ViewPager in activity_main.xml
		mPager = (ViewPager) findViewById(R.id.pager);
		
		// Activate Fragment Manager
		FragmentManager fm = getSupportFragmentManager();

		// Capture ViewPager page swipes
		ViewPager.SimpleOnPageChangeListener ViewPagerListener = new ViewPager.SimpleOnPageChangeListener() {
			@Override
			public void onPageSelected(int position) {
				super.onPageSelected(position);
				// Find the ViewPager Position
				mActionBar.setSelectedNavigationItem(position);
			}
		};

		mPager.setOnPageChangeListener(ViewPagerListener);
		// Locate the adapter class called ViewPagerAdapter.java
		ViewPagerAdapter viewpageradapter = new ViewPagerAdapter(fm);
		// Set the View Pager Adapter into ViewPager
		mPager.setAdapter(viewpageradapter);
		
		// Capture tab button clicks
		ActionBar.TabListener tabListener = new ActionBar.TabListener() {

			@Override
			public void onTabSelected(Tab tab, FragmentTransaction ft) {
				// Pass the position on tab click to ViewPager
				mPager.setCurrentItem(tab.getPosition());
			}

			@Override
			public void onTabUnselected(Tab tab, FragmentTransaction ft) {
				// TODO Auto-generated method stub
			}

			@Override
			public void onTabReselected(Tab tab, FragmentTransaction ft) {
				// TODO Auto-generated method stub
			}
		};
		
		// Create first Tab
		tab = mActionBar.newTab().setTabListener(tabListener).setIcon(R.drawable.ltvicon);
		mActionBar.addTab(tab);
		
		// Create second Tab
		tab = mActionBar.newTab().setTabListener(tabListener).setIcon(R.drawable.chans);
		mActionBar.addTab(tab);
		
		// Create third Tab
		tab = mActionBar.newTab().setText("my").setTabListener(tabListener);
		mActionBar.addTab(tab);
		
		tab = mActionBar.newTab().setText("4").setTabListener(tabListener);
		mActionBar.addTab(tab);

	}
	
	@Override
		public boolean onCreateOptionsMenu(Menu menu) {
			com.actionbarsherlock.view.MenuInflater inflater = getSupportMenuInflater();
			inflater.inflate(R.menu.main, (com.actionbarsherlock.view.Menu) menu);
			return super.onCreateOptionsMenu(menu);
	}
	
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		case android.R.id.home:
				LeftSliderMenu.toggleLeftDrawer();
			}
		return super.onOptionsItemSelected(item);
		}
	
	@Override
	public boolean onPrepareOptionsMenu(Menu menu) {
		MenuItem actionViewItem = menu.findItem(R.id.av);
		View v = actionViewItem.getActionView();
		ImageButton addSceneNotesBtn = (ImageButton) v.findViewById(R.id.addSceneNotes_btn);
		ImageButton addSceneAVBtn = (ImageButton) v.findViewById(R.id.addSceneAV_btn);

		addSceneNotesBtn.setOnClickListener(new Button.OnClickListener(){

	    	  @Override
	    	  public void onClick(View arg0) {
	    		  
	    		  Intent intent = new Intent(MainActivity.this, DirectSceneNote.class);
	              startActivity(intent);
	    		  
	    	  }	
	     }); 
		addSceneAVBtn.setOnClickListener(new Button.OnClickListener(){

	    	  @Override
	    	  public void onClick(View arg0) {
	    		  
					Intent intent = new Intent(Intent.ACTION_GET_CONTENT);
					intent.setType("*/*");
					intent.addCategory(Intent.CATEGORY_OPENABLE	);
					startActivityForResult(
							Intent.createChooser(intent, "Select a Scene to Add"),
							FILE_SELECT_CODE);
	    		  
	    	  }	
	     });
		return super.onPrepareOptionsMenu(menu);
	}
	
	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		if (requestCode == FILE_SELECT_CODE) {
			if (resultCode == RESULT_OK) {  
                // get path of selected file 
                Uri uri = data.getData();
                String path = getPathFromContentUri(uri);
                Log.d("S3", "uri=" + uri.toString());
                Log.d("S3", "path=" + path);
                // initiate the upload
                Intent intent = new Intent(this, SceneInfo.class);
                intent.putExtra("UploadService.ARG_FILE_PATH", path);
				intent.putExtra("Queue", "allNewOriginalScene");
				intent.putExtra("Type", "originalScene");
				
                startActivity(intent);
            }
		}
		super.onActivityResult(requestCode, resultCode, data);
	}
	
	private String getPathFromContentUri(Uri uri) {
		String path = uri.getPath();
		if (uri.toString().startsWith("content://")) {
			String[] projection = { MediaStore.MediaColumns.DATA };
			ContentResolver cr = getApplicationContext().getContentResolver();
			Cursor cursor = cr.query(uri, projection, null, null, null);
			if (cursor != null) {
				try {
					if (cursor.moveToFirst()) {
						path = cursor.getString(0);
					}
				} finally {
					cursor.close();
				}
			}

		}
		return path;
	}

    @Override public void onResume() {
        super.onResume();        
        
    }
    
    @Override public void onPause() {
        super.onPause();        
        
    }
    
	 private void customToast(String message) {
		 LayoutInflater li = getLayoutInflater();
		 View toastLayout = li.inflate(R.layout.toast, (ViewGroup)findViewById
				 (R.id.toastLayout));
		 
	 	 TextView text = (TextView) toastLayout.findViewById(R.id.toastText);
	 	 text.setText(message);
	 	 Toast toast = new Toast(this);
	 	 toast.setDuration(Toast.LENGTH_LONG);
	 	 toast.setView(toastLayout);
	 	 toast.show();
	 }
	

}


