package tv.loqoo.v4;

import java.io.IOException;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;

import com.fasterxml.jackson.core.sym.Name;
import com.loopj.android.image.SmartImageView;
import com.parse.PushService;

import tv.loqoo.v4.R;
import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.content.pm.ActivityInfo;
import android.net.Uri;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.provider.MediaStore;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ProgressBar;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

public class SceneInfo extends Activity {

	TextView SceneLink;
	EditText SceneName;
	EditText SceneDescp;
	EditText SceneTags1;
	EditText ScenePrice;
	EditText SceneTags3;
	ImageButton SceneUploadBtn;
	String myLastSceneName;
	String myLastSceneDescp;
	String myLastSceneTags1;
	String myLastScenePrice;
	String myLastSceneTags3;
	String myLastScenePriceDnom;
	String myLastSendToChannel;
	Spinner bitcoinTag, myChannelsSpinner;
	List<String> bitcoinArray = new ArrayList<String>();
	ProgressBar SceneUploadProgress;
	List<String> channelsArray;
	

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.sceneinfo);
		if (getResources().getBoolean(R.bool.portrait_only)) {
			setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
		}
		
		SceneLink = (TextView) findViewById(R.id.scinfoSceneLink);
		SceneName = (EditText) findViewById(R.id.scinfoSceneName);
		SceneDescp = (EditText) findViewById(R.id.scinfoSceneDescp);
		SceneTags1 = (EditText) findViewById(R.id.scinfoSceneTags1);
		ScenePrice = (EditText) findViewById(R.id.scinfoSceneTags2);
		SceneTags3 = (EditText) findViewById(R.id.scinfoSceneTags3);
		bitcoinTag = (Spinner) findViewById(R.id.priceit);
		myChannelsSpinner = (Spinner) findViewById(R.id.myChannels);
		SceneUploadProgress = (ProgressBar) findViewById(R.id.scinfoSceneProgressBar);
		SceneUploadBtn = (ImageButton) findViewById(R.id.scinfoSceneUploadBtn);
		
		SharedPreferences sharedPrefs = getSharedPreferences("networkInfo", 0);
		String networkName = (sharedPrefs.getString("networkName", ""));

		Log.d("LOQ/TV.sceneinfo.networkName", networkName);

		channelsArray = new ArrayList<String>();
		channelsArray.add("ok");
		channelsArray.add("ko");
		
		LoqooApplication application = (LoqooApplication) this.getApplication();
		final List<String> channels = new ArrayList<String>(application.channels);
		for (String o : channels)
			System.out.println(o);
			System.out.println(channels);
		
		
		ArrayAdapter arrayAdapter = new ArrayAdapter(this,
				android.R.layout.simple_spinner_item, channels);
		myChannelsSpinner.setAdapter(arrayAdapter);	
		myChannelsSpinner.setOnItemSelectedListener( new AdapterView.OnItemSelectedListener() {
				public void onItemSelected(AdapterView<?> adapterView, View view,
						int i, long l) { 
					String myLastSendToChannel = channels.get(i);
					SharedPreferences sharedPrefs = getSharedPreferences("networkInfo", 0);
					SharedPreferences.Editor editor = sharedPrefs.edit();
					editor.putString("myLastSendToChannel", myLastSendToChannel);
					editor.commit();
					Log.d("LOQ/TV.sceneinfo.sendtochannel", myLastSendToChannel);
				}
				@Override
				public void onNothingSelected(AdapterView<?> arg0) {
					// TODO Auto-generated method stub		
				}
		});
		
		bitcoinArray.add("BTC");
		bitcoinArray.add("mBTC");
		
		ArrayAdapter<String> arrayAdapter1 = new ArrayAdapter<String>(SceneInfo.this,
				android.R.layout.simple_spinner_item, bitcoinArray );
		bitcoinTag.setAdapter(arrayAdapter1);
		bitcoinTag.setOnItemSelectedListener( new AdapterView.OnItemSelectedListener() {
			public void onItemSelected(AdapterView<?> adapterView, View view,
					int i, long l) { 
				
				myLastScenePriceDnom = bitcoinArray.get(i);
				SharedPreferences sharedPrefs = getSharedPreferences("networkInfo", 0);
				SharedPreferences.Editor editor = sharedPrefs.edit();
				editor.putString("myLastScenePriceDnom", myLastScenePriceDnom);
				editor.commit();
				Log.d("LOQ/TV.sceneinfo.bitcoin", myLastScenePriceDnom);
			}

			@Override
			public void onNothingSelected(AdapterView<?> arg0) {
				// TODO Auto-generated method stub
				
			}
			
	});
		
		
		SceneUploadBtn.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				SharedPreferences sharedPrefs = getSharedPreferences("networkInfo", 0);
				String networkName = (sharedPrefs.getString("networkName", ""));
				customToast("Your "+networkName+" Scene is being pushed now...");
				myLastSceneName = SceneName.getText().toString();
				myLastSceneDescp = SceneDescp.getText().toString();
				myLastSceneTags1 = SceneTags1.getText().toString();
				myLastSceneTags3 = SceneTags3.getText().toString();
				myLastScenePrice = ScenePrice.getText().toString();
				SharedPreferences.Editor editor = sharedPrefs.edit();
	    		editor.putString("MyLastSceneName", myLastSceneName);
				editor.putString("myLastSceneDescp", myLastSceneDescp);
				editor.putString("myLastSceneTags1", myLastSceneTags1);
				editor.putString("myLastScenePrice", myLastScenePrice);
				editor.putString("myLastSceneTags3", myLastSceneTags3);
				editor.putString("myLastScenePriceDnom", myLastScenePriceDnom);
				Log.d("LOQ/TV.sceneinfo", myLastSceneName);
				Log.d("LOQ/TV.sceneinfo", myLastSceneDescp);
				Log.d("LOQ/TV.sceneinfo", myLastSceneTags1);
				Log.d("LOQ/TV.sceneinfo", myLastSceneTags3);
				Log.d("LOQ/TV.sceneinfo", myLastScenePriceDnom);
				Log.d("LOQ/TV.sceneinfo", myLastScenePrice);
				editor.commit();
				Intent scene = getIntent();
				String path = scene.getStringExtra("UploadService.ARG_FILE_PATH");
				String queue = scene.getStringExtra("Queue");
				//Log.d("LOQ/TV.sceneinfo.path.intentExtra", path);
				Intent intent = new Intent(SceneInfo.this, UploadService.class);
                intent.putExtra(UploadService.ARG_FILE_PATH, path);
				intent.putExtra("Queue",queue);
                startService(intent);
				Log.d("LOQ/TV.sceneinfo.intentExtra", queue);
				
			}	
			
		});

	}
		
	
	@Override
	protected void onStart() {
		super.onStart();
		IntentFilter f = new IntentFilter();
		f.addAction(UploadService.UPLOAD_STATE_CHANGED_ACTION);
		registerReceiver(uploadStateReceiver, f);
	}

	@Override
	protected void onStop() {
		unregisterReceiver(uploadStateReceiver);
		super.onStop();
	}
	
	private BroadcastReceiver uploadStateReceiver = new BroadcastReceiver() {

		@Override
        public void onReceive(Context context, Intent intent) {
        	Bundle b = intent.getExtras();
        	SceneLink.setText(b.getString(UploadService.MSG_EXTRA));
        	//Log.d("s3Url", b.getString(UploadService.MSG_EXTRA));
        	//myImage.setImageUrl(b.getString(UploadService.MSG_EXTRA));
        	int percent = b.getInt(UploadService.PERCENT_EXTRA);
        	SceneUploadProgress.setIndeterminate(percent < 0);
        	SceneUploadProgress.setProgress(percent);
        }
    };
    
	 private void customToast(String message) {
		 LayoutInflater li = (LayoutInflater) getSystemService( LAYOUT_INFLATER_SERVICE );
		 View toastLayout = li.inflate(R.layout.toast, null );
	 	 TextView text = (TextView) toastLayout.findViewById(R.id.toastText);
	 	 text.setText(message);
	 	 Toast toast = new Toast(this);
	 	 toast.setDuration(Toast.LENGTH_LONG);
	 	 toast.setView(toastLayout);
	 	 toast.show();
	 }
 

	
}
		
