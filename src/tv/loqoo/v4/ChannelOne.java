package tv.loqoo.v4;


import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.actionbarsherlock.app.SherlockFragment;
import com.bumptech.glide.Glide;
import com.firebase.client.Firebase;
import com.squareup.otto.Subscribe;

public class ChannelOne extends SherlockFragment {
	public static String incomingScene = null;
	public static String myLastSceneUrl2 = null;
	ImageView myLastSceneImage;
	String dUrl = "http://loqoonet.com/loqootv/LTVlogo.png";
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		// Get the view from fragmenttab3.xml
		View view = inflater.inflate(R.layout.channelone, container, false);
		
		myLastSceneImage = (ImageView) view.findViewById(R.id.myLastScene);
		SharedPreferences sharedPrefs = getActivity().getSharedPreferences("IncomingSceneInfo", 0);
		myLastSceneUrl2 = (sharedPrefs.getString("myLastIncomingSceneUrl", ""));
		
		System.out.println("incomingscene"+incomingScene);
		System.out.println("mylastscene"+myLastSceneUrl2);
		try {
		Glide.load(incomingScene).into(myLastSceneImage);
		} catch(Exception e) {	
		System.out.println(e);
		}
		return view;
	}

	@Override
    public void onActivityCreated(Bundle savedInstanceState) {
    	super.onActivityCreated(savedInstanceState);
    	
    }

	@Override
	public void onSaveInstanceState(Bundle outState) {
		super.onSaveInstanceState(outState);
		setUserVisibleHint(true);
	}
	
	
    @Override public void onResume() {
        super.onResume();        

    }
    
    @Override public void onPause() {
        super.onPause(); 

    }
    
    

}