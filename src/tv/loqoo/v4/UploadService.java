package tv.loqoo.v4;

import java.io.File;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Arrays;
import java.util.NoSuchElementException;
import java.util.StringTokenizer;

import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.services.s3.AmazonS3Client;
import com.amazonaws.services.s3.model.ProgressEvent;
import com.readystatesoftware.simpl3r.UploadIterruptedException;
import com.readystatesoftware.simpl3r.Uploader;
import com.readystatesoftware.simpl3r.Uploader.UploadProgressListener;

import android.app.IntentService;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v4.app.NotificationCompat;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;
import android.text.TextUtils;
import android.app.Activity;

public class UploadService extends IntentService {

	public static final String ARG_FILE_PATH = "file_path";
	public static final String UPLOAD_STATE_CHANGED_ACTION = "com.readystatesoftware.simpl3r.example.UPLOAD_STATE_CHANGED_ACTION";
	public static final String UPLOAD_CANCELLED_ACTION = "com.readystatesoftware.simpl3r.example.UPLOAD_CANCELLED_ACTION";
	public static final String S3KEY_EXTRA = "s3key";
	public static final String PERCENT_EXTRA = "percent";
	public static final String MSG_EXTRA = "msg";
	public static String lastSceneUrl;
	private static final int NOTIFY_ID_UPLOAD = 1337;
	public static String queue;
	public static String myLastSceneName;
	public static String networkName;
	public static String networkEmail;
	public static String myLastSceneDescp;
	public static String myLastSceneTags1;
	public static String myLastScenePrice;
	public static String myLastSceneTags3;
	public static String myLastSceneTags4;
	public static String myLastSendToChannel;
	public static String myLastScenePriceDnom;
	public static String sceneType;

	private AmazonS3Client s3Client;
	private Uploader uploader;

	private NotificationManager nm;

	public UploadService() {
		super("simpl3r-example-upload");
	}

	@Override
	public void onCreate() {
		super.onCreate();


		s3Client = new AmazonS3Client(
			new BasicAWSCredentials(getString(R.string.s3_access_key), getString(R.string.s3_secret)));
		nm = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);

		IntentFilter f = new IntentFilter();
		f.addAction(UPLOAD_CANCELLED_ACTION);
		registerReceiver(uploadCancelReceiver, f);
	}	

	@Override
	protected void onHandleIntent(Intent intent) {

		String filePath = intent.getStringExtra(ARG_FILE_PATH);
		String queue = intent.getStringExtra("Queue");
		Log.d("S3.filePath", filePath);
		String fileType = filePath.substring(filePath.length() - 4);
		Log.d("S3.fileType", fileType);
		String FileExt = null;
		try {
			StringTokenizer tokens = new StringTokenizer(filePath, ".");
			String first = tokens.nextToken();
			FileExt = tokens.nextToken();
		}catch(NoSuchElementException e){
			customToast("the scene you chose, has no extension :(");
		}
		System.out.println("EXT " +FileExt);
		File fileToUpload = new File(filePath);
        SharedPreferences sharedPrefs = getSharedPreferences("networkInfo", 0);
		networkName = (sharedPrefs.getString("networkName", ""));
		networkEmail = (sharedPrefs.getString("networkEmail", ""));
		myLastSceneName = (sharedPrefs.getString("myLastSceneName", ""));
		myLastSceneDescp = (sharedPrefs.getString("myLastSceneDescp", ""));
		myLastSceneTags1 = (sharedPrefs.getString("myLastSceneTags1", ""));
		myLastScenePrice = (sharedPrefs.getString("myLastScenePrice", ""));
		myLastSceneTags3 = (sharedPrefs.getString("myLastSceneTags3", ""));
		myLastSendToChannel = (sharedPrefs.getString("myLastSendToChannel", ""));
		myLastScenePriceDnom = (sharedPrefs.getString("myLastScenePriceDnom", ""));
		String[] image = {"jpg", "png", "gif", "aiff", "jpeg", "jpe", "webp", "svg"};
		String[] audio = {"amr", "mp3", "wav"};
		String[] video = {"mp4", "3gp", "3gpp", "mov", "mpv", "flv", };
		String text = "txt";
		String sceneType = null;
		if (Arrays.asList(image).contains(FileExt)) {
			sceneType = "originalImage";
		}else if(Arrays.asList(audio).contains(FileExt)) {
			sceneType = "originalAudio";
		}else if(Arrays.asList(video).contains(FileExt)) {
			sceneType = "originalVideo";
		}else if(FileExt == text) {
			sceneType = "originalText";
		}else {
			sceneType = FileExt;
		}
		final String s3ObjectKey = networkName+"_"+md5(filePath)+"."+FileExt;
		Log.d("S3.s3objectkey", s3ObjectKey);
		String s3BucketName = getString(R.string.s3_bucket);

		final String msg = "Uploading " + s3ObjectKey + "...";

		// create a new uploader for this file
		uploader = new Uploader(this, s3Client, s3BucketName, s3ObjectKey, fileToUpload);
		Log.d("S3.s3bucketname.s3objectkey", s3BucketName+s3ObjectKey);

		// listen for progress updates and broadcast/notify them appropriately
		uploader.setProgressListener(new UploadProgressListener() {			
				@Override
				public void progressChanged(ProgressEvent progressEvent,
											long bytesUploaded, int percentUploaded) {

					Notification notification = buildNotification(msg, percentUploaded);
					nm.notify(NOTIFY_ID_UPLOAD, notification);
					broadcastState(s3ObjectKey, percentUploaded, msg);
				}
			});

		// broadcast/notify that our upload is starting
		Notification notification = buildNotification(msg, 0);
		nm.notify(NOTIFY_ID_UPLOAD, notification);
		broadcastState(s3ObjectKey, 0, msg);

		try {
			String s3Location = uploader.start(); // initiate the upload
			broadcastState(s3ObjectKey, -1, "File successfully uploaded to " + s3Location);
			Log.d("S3.s3location", s3Location);
			String projectId = getString(R.string.ironmq_project_id);
			String token = getString(R.string.ironmq_token);

			String message = "networkName:"+networkName+";sceneType:"+sceneType+";"+"sceneUrl:"+s3Location+";sceneName:"+myLastSceneName+";"+"sceneDescp:"+myLastSceneDescp+";"+"sceneTag1:"
				+myLastSceneTags1+";sceneTag3:"+myLastSceneTags3+";scenePrice:"+myLastScenePrice+";scenePriceDnom:"+myLastScenePriceDnom+";sendToChannel:"+myLastSendToChannel+";" +
						"networkEmail:"+networkEmail+";timestamp:"+System.currentTimeMillis()+";eventType:"+"pushScene"+";end";
			//log scene to catch-all queues ( originalScene, youtubeScene, etc )
			IronMQ task = new IronMQ();
			task.projectId = projectId;
			task.token = token;
			task.q = queue;
			task.message = message;
			task.execute();
			//log scene to channel's queue
			IronMQ task1 = new IronMQ();
			task1.projectId = projectId;
			task1.token = token;
			task1.q = "/channel/"+myLastSendToChannel;
			task1.message = message;
			task1.execute();
			//log scene to network's queue
			IronMQ task2 = new IronMQ();
			task2.projectId = projectId;
			task2.token = token;
			task2.q = "/network/"+networkName;
			task2.message = message;
			task2.execute();

		} catch (UploadIterruptedException uie) {
			broadcastState(s3ObjectKey, -1, "User interrupted");
		} catch (Exception e) {
			e.printStackTrace();
			broadcastState(s3ObjectKey, -1, "Error: " + e.getMessage());
		}
	}
	
	@Override
	public void onDestroy() {
		nm.cancel(NOTIFY_ID_UPLOAD);
		unregisterReceiver(uploadCancelReceiver);
		super.onDestroy();
	}

	private void broadcastState(String s3key, int percent, String msg) {
		Intent intent = new Intent(UPLOAD_STATE_CHANGED_ACTION);
		Bundle b = new Bundle();
		b.putString(S3KEY_EXTRA, s3key);
		b.putInt(PERCENT_EXTRA, percent);
		b.putString(MSG_EXTRA, msg);
		intent.putExtras(b);
		sendBroadcast(intent);
	}

	private Notification buildNotification(String msg, int progress) {	
		NotificationCompat.Builder builder = new NotificationCompat.Builder(this);
		builder.setWhen(System.currentTimeMillis());
		builder.setTicker(msg);
		builder.setContentTitle(getString(R.string.app_name));
		builder.setContentText(msg);
		builder.setSmallIcon(R.drawable.ltvicon);
		builder.setOngoing(true);
		builder.setProgress(100, progress, false);
		
		Intent notificationIntent = new Intent(this, MainActivity.class);
		notificationIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
		PendingIntent contentIntent = PendingIntent.getActivity(this, 0, notificationIntent, 0);
		builder.setContentIntent(contentIntent);
		
		return builder.build();
	}
	
	private BroadcastReceiver uploadCancelReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
        	if (uploader != null) {
        		uploader.interrupt();
        	}
        }
    };
	
	private String md5(String s) {
		try {
			// create MD5 Hash
			MessageDigest digest = java.security.MessageDigest.getInstance("MD5");
			digest.update(s.getBytes());
			byte messageDigest[] = digest.digest();

			// create Hex String
			StringBuffer hexString = new StringBuffer();
			for (int i=0; i<messageDigest.length; i++)
				hexString.append(Integer.toHexString(0xFF & messageDigest[i]));
			return hexString.toString();

		} catch (NoSuchAlgorithmException e) {
			e.printStackTrace();
			return null;
		}
	}
	
	 private void customToast(String message) {
		 LayoutInflater li = (LayoutInflater) getSystemService( LAYOUT_INFLATER_SERVICE );
		 View toastLayout = li.inflate(R.layout.toast, null );
	 	 TextView text = (TextView) toastLayout.findViewById(R.id.toastText);
	 	 text.setText(message);
	 	 Toast toast = new Toast(this);
	 	 toast.setDuration(Toast.LENGTH_LONG);
	 	 toast.setView(toastLayout);
	 	 toast.show();
	 }
	

}
