package tv.loqoo.v4;

import tv.loqoo.v4.R;
import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;

import org.json.JSONObject;
import org.json.JSONException;

import android.preference.PreferenceManager;
import android.util.Log;
import android.net.*;

public class LoqooBroadcast extends BroadcastReceiver {

	private static final String TAG = "LoqootvBroadcast";
	String test;
	
		@Override
		public void onReceive(Context context, Intent intent) {
			String action = intent.getAction();
			Log.d("broadcasting", "broadcasting....");
			if (action.equalsIgnoreCase("tv.loqoo.v4.ORIGINAL_WEB_SCENE")) {
		try {
			Log.d("broadcasting", "broadcasting....");
			String channel = intent.getExtras().getString( "com.parse.Channel");
			JSONObject json = new JSONObject(intent.getExtras().getString("com.parse.Data"));
			String incomingScene = json.getString("url");
			Log.d("LoqooBroadcast.incomingScene",incomingScene);
			SharedPreferences sharedPrefs = PreferenceManager.getDefaultSharedPreferences(context);
        	SharedPreferences.Editor editor = sharedPrefs.edit();
        	editor.putString("myLastIncomingSceneUrl", incomingScene);
        	Log.d("LoqooBroadcast.sharedPref",incomingScene);
        	editor.commit();

			}catch(JSONException e) {
			};
				}else if (action.equalsIgnoreCase("tv.loqoo.v4.ORIGINAL_VIDEO_SCENE")) {
					try {
						Log.d("broadcasting", "broadcasting....");
						String channel = intent.getExtras().getString( "com.parse.Channel");
						JSONObject json = new JSONObject(intent.getExtras().getString("com.parse.Data"));
						String incomingScene = json.getString("url");
						Log.d("LoqooBroadcast.incomingScene",incomingScene);
						
						SharedPreferences sharedPrefs = context.getSharedPreferences("IncomingSceneInfo", 0);
						SharedPreferences.Editor editor = sharedPrefs.edit();
						editor.putString("myLastIncomingSceneUrl", incomingScene);
						Log.d("LoqooBroadcast.sharedPref",incomingScene);
						editor.commit();
						Log.d("broadcasting", "broadcasting....");
						Uri data = Uri.parse(incomingScene);
						Intent intent1 = new Intent(context,Videoview.class);
						intent1.putExtra("extra_text", incomingScene); 
						context.startActivity(intent1);
						

					} catch(JSONException e) {

					};
					
					
					
				}else if (intent.getAction().equalsIgnoreCase("tv.loqoo.v4.ORIGINAL_IMAGE_SCENE")) {
					try {
						Log.d("broadcasting", "broadcasting....");
						String channel = intent.getExtras().getString( "com.parse.Channel");
						Log.d("LoqooBroadcast.incomingScene.channel",channel);
						JSONObject json = new JSONObject(intent.getExtras().getString("com.parse.Data"));
						String incomingScene = json.getString("url");
						Log.d("LoqooBroadcast.incomingScene.url",incomingScene);
						SharedPreferences sharedPrefs = PreferenceManager.getDefaultSharedPreferences(context);
						SharedPreferences.Editor editor = sharedPrefs.edit();
						editor.putString("lastScene", incomingScene);
						ChannelOne.incomingScene = incomingScene;
						Log.d("LoqooBroadcast.sharedPref",incomingScene);
						editor.commit();
						Log.d("broadcasting", "broadcasting....");
						} catch(JSONException e) {
							Log.d("incoming.ORIG.IMAGE_SCENE", "JSONException");
							System.out.println(e);
						};
					
					
				}else if (intent.getAction().equalsIgnoreCase("tv.loqoo.v4.ORIGINAL_AUDIO_SCENE")) {
					
				}else if (intent.getAction().equalsIgnoreCase("tv.loqoo.v4.THIRDPARTY_AUDIO_SCENE")) {
					
				}else if (intent.getAction().equalsIgnoreCase("tv.loqoo.v4.THIRDPARTY_VIDEO_SCENE")) {
					
				}else if (intent.getAction().equalsIgnoreCase("tv.loqoo.v4.THIRDPARTY_IMAGE_SCENE")) {
					
				}else if (intent.getAction().equalsIgnoreCase("tv.loqoo.v4.THIRDPARTY_PLAYSTORE_SCENE")) {
					
				}else if (intent.getAction().equalsIgnoreCase("tv.loqoo.v4.THIRDPARTY_WEB_SCENE")) {

				}else if (intent.getAction().equalsIgnoreCase("tv.loqoo.v4.CONVO")) {
				
				}
				else if (intent.getAction().equalsIgnoreCase("tv.loqoo.v4.ORIGINAL_SCENE")) {
					try {
					Log.d("broadcasting", "broadcasting....");
					String channel = intent.getExtras().getString( "com.parse.Channel");
					//Log.d("LoqooBroadcast.incomingScene",channel);
					JSONObject json = new JSONObject(intent.getExtras().getString("com.parse.Data"));
					String incomingScene = json.getString("url");
					//Log.d("LoqooBroadcast.incomingScene",incomingScene);
					SharedPreferences sharedPrefs = context.getSharedPreferences("userInfo", 0);
					SharedPreferences.Editor editor = sharedPrefs.edit();
					editor.putString("lastScene", incomingScene);
					Log.d("LoqooBroadcast.sharedPref",incomingScene);
					editor.commit();
					} catch(JSONException e) {

					};
				}
		
		

	}

}
