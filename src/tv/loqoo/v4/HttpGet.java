package tv.loqoo.v4;


import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

import android.os.AsyncTask;
import android.util.Log;


public class HttpGet extends AsyncTask<String, String, String> {
                
                @Override
                protected void onPreExecute() {
                    super.onPreExecute();
                }
                
                @Override
                protected String doInBackground(String... shareUrl) {
                        HttpURLConnection con = null;

                        try {
                               URL url = new URL(shareUrl[0]);         
                               con = (HttpURLConnection) url.openConnection();
                               con.setConnectTimeout(500);
                               con.setReadTimeout(100);
                               int response = con.getResponseCode();
                               Log.d("httpGet.ResponseCode",Integer.toString(response));
                               readStream(con.getInputStream());
                               } catch (Exception e) {
                               e.printStackTrace();
                			   }finally {
                                                        if (con != null) {
                                        con.disconnect();
                                    }
                                }
                                        return null;
                            }

                             private void readStream(InputStream in) {
                               BufferedReader reader = null;
                           try {
                             reader = new BufferedReader(new InputStreamReader(in));
                             String line = "";
                             while ((line = reader.readLine()) != null) {
                             System.out.println(line);
                             Log.d("httpGet.Response",line);
                             reader.close();}
                             }catch (IOException e)
                             {e.printStackTrace();}
                             }
                             

                             

            }